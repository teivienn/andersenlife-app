import React, { Component, PureComponent } from "react";
import { StyleSheet, Text, View, StatusBar } from "react-native";

import AppContainer from "./src/navigation/AppContainer";

interface Props {}
export default class App extends PureComponent<Props> {
  render() {
    return <AppContainer />;
  }
}
