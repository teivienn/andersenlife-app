import React, { Component, PureComponent } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  StatusBar,
  TextInput,
  TouchableOpacity
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

interface Props {
  leftColor: string,
  rightColor: string,
  text: string,
  onPress?: any,
}
export default class CustomButton extends PureComponent<Props> {

  constructor(props: Props) {
    super(props);
  }


  render() {

    const {leftColor, rightColor, text} = this.props;
    return (
      <View>
        <TouchableOpacity
          onPress={() => this.props.onPress()}
          style={styles.touch_button}
          >
          <LinearGradient
            colors={[leftColor, rightColor]}
            start={{x: 0, y: 0.5}}
            end={{x: 1, y: 0.5}}
            style={styles.gradient}>
            <Text style={styles.text_button}>
              {text}
            </Text>
          </LinearGradient>
        </TouchableOpacity>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  touch_button: {
    marginBottom: 10,
    borderRadius: 30,
    width: 300,
    height: 50,
  },
  gradient: {
    padding: 15,
    borderRadius: 30
  },
  text_button: {
    textAlign: 'center',
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 15,
  },
})
