import React, { Component, PureComponent } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  StatusBar,
  TouchableOpacity
} from 'react-native';

const { width } = Dimensions.get('window');

// @ts-ignore
import backImg from '../assets/back.png';

interface IProps {
  text: string,
}

export default class CustomHeader extends PureComponent<IProps> {
  constructor(props: IProps) {
    super(props)
  }



  render() {
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor="#FEDA00" barStyle="dark-content" />
        <Text style={styles.title}>{this.props.text}</Text>
      </View>
    )
  }

};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FEDA00',
    flex: 0.1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row'
  },
  title: {
    fontWeight: 'bold',
    fontSize: 18,
    color: '#000',
  }
});