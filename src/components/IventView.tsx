import React, { Component, PureComponent } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  StatusBar,
  TouchableOpacity
} from 'react-native';
import moment from 'moment';
import _url from '../services/url'


const { width } = Dimensions.get('window');
const img = require('./../assets/users.png')

interface Props {
  date: string,
  id: number,
  image: string,
  description: string,
  title: string,
  user: number,
  openNews?: any
}
export default class IventView extends PureComponent<Props>{

  constructor(props: Props) {
    super(props)
  }


  viewPhoto(image: string) {
    return `${_url}/${image}`
  }
  // @ts-ignore
  dateFormat(date: string) {
    return moment(date).format('DD.MM.YYYY');
  }

  render() {
    let {date, id, image, description, title, user} = this.props;

    return (
      <View style={styles.container}>
        <TouchableOpacity
          onPress={() => this.props.openNews(id, title)}
          style={styles.touch}>

          <Image
            style={styles.image}
            source={{uri: this.viewPhoto(image)}}/>

          <View style={styles.body_cont}>
            <View style={styles.title_container}>
              <Text style={styles.title} numberOfLines={1}>{title}</Text>
            </View>
            <View style={styles.description_cont}>
              <Text style={styles.description} numberOfLines={3}>{description}</Text>
            </View>
          </View>

          <View style={styles.date_cont} >
            <Text>{this.dateFormat(date)}</Text>
          </View>
          <View style={styles.user_cont} >
            <Text>{user}</Text>
            <Image
              style={styles.image_user}
              source={img} />
          </View>

        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  touch: {
    marginTop: 5,
    flexDirection: 'row',
    marginBottom: 5,
    width: '98%',
    backgroundColor: '#ffff', //#f9f9f9
    borderRadius: 100,
    // shadowColor: '#000',
    // shadowOffset: {
    //   width: 0,
    //   height: 1
    // },
    // shadowOpacity: 1,
    // shadowRadius: 1.41,
    elevation: 3

  },
  image: {
    // marginLeft: 2,
    height: width * .3,
    width: width * .3,
    borderRadius: 100,
  },
  body_cont: {
    marginLeft: 25,
    // flexDirection: 'row',
    // alignItems: 'center',
    justifyContent: 'space-evenly',
  },
  title_container: {
    // height: 40,
    // alignItems: 'center',
    // justifyContent: 'center',
    // padding: 5,

  },
  title: {
    fontWeight: 'bold',
    fontSize: 16,
    color: '#000',
  },
  description_cont: {
    width: width * .5,
    // height: 60,
    // alignItems: 'center',
    // justifyContent: 'center',
  },
  description: {
    // textAlign:'center',
    // padding: 5,
    // color: '#000',
  },
  date_cont: {
    backgroundColor: 'rgba(196, 196, 196, 0.8)',
    borderRadius: 30,
    padding: 6,
    position: 'absolute',
    top: width * .215,
    left: 45
  },
  user_cont: {
    alignItems: 'center',
    justifyContent: 'space-around',
    flexDirection: 'row',
    backgroundColor: 'rgba(196, 196, 196, 0.8)',
    borderRadius: 30,
    padding: 6,
    width: 60,
    position: 'absolute',
    top: width * .0,
    left: 45
  },
  image_user: {
    width: 15,
    height: 15
  }
});