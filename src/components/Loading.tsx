import React, { Component, PureComponent } from 'react';
import { 
  StyleSheet, 
  Text, 
  View, 
  Dimensions, 
  StatusBar,
  ToastAndroid
} from 'react-native';
import { BarIndicator } from 'react-native-indicators';
const { width } = Dimensions.get('window');

export default class loading extends PureComponent {

  render() {

    return (
      <View style={styles.container}>
        <BarIndicator color='#FEDA00' />
      </View>
    );
  }
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  }
});