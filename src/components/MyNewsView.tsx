import React, { Component, PureComponent } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  StatusBar,
  TouchableOpacity
} from 'react-native';
import moment from 'moment';
import _url from '../services/url'

const { width } = Dimensions.get('window');
interface Props {
  date: string,
  id: number,
  image: string,
  description: string,
  title: string,
  openNews?: any
}
export default class MyNewsView extends PureComponent<Props>{

  constructor(props: Props) {
    super(props)
  }


  viewPhoto(image: string) {
    return `${_url}/${image}`
  }

  dateFormat(date: string) {
    return moment(date).format('DD.MM.YYYY');
  }

  render() {
    let {date, id, image, description, title} = this.props;

    return (
      <View style={styles.container}>
        <TouchableOpacity
          onPress={() => this.props.openNews(id, title)}
          style={styles.touch}>
          <View style={styles.title_container}>
            <Text style={styles.title} numberOfLines={1}>{title}</Text>
          </View>
          <Image
            style={styles.image}
            source={{uri: this.viewPhoto(image)}}/>
          <View style={styles.description_cont}>
            <Text style={styles.description} numberOfLines={3}>{description}</Text>
          </View>
          <View style={styles.date_cont} >
            <Text>{this.dateFormat(date)}</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  touch: {
    marginTop: 5,
    marginBottom: 5,
    width: '98%',
    backgroundColor: '#ffff', //#f9f9f9
    borderRadius: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowOpacity: 1,
    shadowRadius: 1.41,
    elevation: 3

  },
  image: {
    height: width * .5,
    width: '100%',
  },
  title_container: {
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 5,

  },
  title: {

    fontWeight: 'bold',
    fontSize: 16,
    color: '#000',
  },
  description_cont: {
    height: 60,
    alignItems: 'center',
    justifyContent: 'center',
  },
  description: {
    textAlign:'center',
    padding: 5,
    color: '#000',
  },
  date_cont: {
    backgroundColor: 'rgba(196, 196, 196, 0.8)',
    borderRadius: 30,
    padding: 8,
    position: 'absolute',
    top: width * .13,
    right: 5
  }
});