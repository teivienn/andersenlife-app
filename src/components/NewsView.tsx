import React, { Component, PureComponent } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  StatusBar,
  TouchableOpacity,
} from 'react-native';
import moment from 'moment';
import _url from '../services/url';

const { width } = Dimensions.get('window');
interface Props {
  date: string;
  id: number;
  image: string;
  description: string;
  title: string;
  openNews?: any;
  uava: string;
  uname: string;
  usurname: string;
}
export default class NewsView extends PureComponent<Props> {
  constructor(props: Props) {
    super(props);
  }

  static viewPhoto(image: string) {
    return `${_url}/${image}`;
  }
  // @ts-ignore
  dateFormat(date: string) {
    return moment(date).format('DD.MM.YYYY');
  }

  static ava(image: string): any {
    if (image == null) {
      return this.viewPhoto('defalt_user.png');
    } else {
      return this.viewPhoto(image);
    }
  }

  render() {
    let {
      date,
      id,
      image,
      description,
      title,
      uava,
      uname,
      usurname,
    } = this.props;

    return (
      <View style={styles.container}>
        <TouchableOpacity
          onPress={() => this.props.openNews(id, title)}
          style={styles.touch}
        >
          <View style={styles.title_container}>
            <Text style={styles.title} numberOfLines={1}>
              {title}
            </Text>
          </View>
          <Image
            style={styles.image}
            source={{ uri: NewsView.viewPhoto(image) }}
          />
          <View style={styles.description_cont}>
            <Text style={styles.description} numberOfLines={3}>
              {description}
            </Text>
          </View>
          <View style={styles.date_cont}>
            <Text>{this.dateFormat(date)}</Text>
          </View>
          <View style={styles.absol}>
            <Image style={styles.uava} source={{ uri: NewsView.ava(image) }} />
            <View style={styles.u_text}>
              <Text style={styles.uu_text}>
                {uname} {usurname}
              </Text>
            </View>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  touch: {
    marginTop: 5,
    marginBottom: 5,
    width: '98%',
    backgroundColor: '#ffff', //#f9f9f9
    borderRadius: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 1,
    shadowRadius: 1.41,
    elevation: 3,
  },
  image: {
    height: width * 0.5,
    width: '100%',
  },
  title_container: {
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 5,
  },
  title: {
    fontWeight: 'bold',
    fontSize: 16,
    color: '#000',
  },
  description_cont: {
    height: 60,
    alignItems: 'center',
    justifyContent: 'center',
  },
  description: {
    textAlign: 'center',
    padding: 5,
    color: '#000',
  },
  date_cont: {
    backgroundColor: 'rgba(196, 196, 196, 0.8)',
    borderRadius: 30,
    padding: 8,
    position: 'absolute',
    top: width * 0.13,
    right: 5,
  },
  absol: {
    left: 5,
    top: width * 0.13,
    flexDirection: 'row',
    position: 'absolute',
  },
  uava: {
    width: width * 0.1,
    height: width * 0.1,
    borderRadius: 30,
  },
  u_text: {
    backgroundColor: 'rgba(196, 196, 196, 0.8)',

    marginLeft: 5,
    borderRadius: 30,
    padding: 8,
  },
  uu_text: {
    color: '#000',
  },
});
