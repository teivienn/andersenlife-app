import React, { PureComponent } from "react";
import { View, StyleSheet, Dimensions } from "react-native";
import SvgUri from 'react-native-svg-from-uri';

const {width} = Dimensions.get('window');

interface IProps {
  height: number,
  width: number,
  source: any;
};
export default class SVGImage extends PureComponent<IProps> {
  constructor(props: IProps) {
    super(props)
  }

  render() {
    const { height, width, source } = this.props;
      return <SvgUri 
        width={this.props.width} 
        height={this.props.height} 
        source={this.props.source}/>
  }
}
