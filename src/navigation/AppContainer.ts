import { createStackNavigator, createAppContainer } from 'react-navigation';
import Tabs from './TabContainer';
import Splash from '../screens/Splash/Splash';
import Login from '../screens/Login/Login';
import Reg from '../screens/Login/Reg';
import RegNext from '../screens/Login/RegNext';
import FullNews from '../screens/News/FullNews';
import AddNews from '../screens/News/AddNews';
import EditProfile from '../screens/Profile/Edit';
import MyNews from '../screens/Profile/MyNews';
import MyIvents from '../screens/Profile/MyIvents';
import MyJoingIvent from '../screens/Profile/MyJoingIvent';
import AddIvent from '../screens/Ivents/AddIvent';
import FullIvent from '../screens/Ivents/FullIvent';
import UserIvint from '../screens/Ivents/UserIvent';

const Navigate = createStackNavigator({
  Splash: Splash,
  Login: {
    screen: Login,
    navigationOptions: {
      title: 'Авторизация',
      headerStyle: {
        backgroundColor: '#FEDA00',
        elevation: 0,
      },
    },
  },
  Reg: {
    screen: Reg,
    navigationOptions: {
      title: 'Регистрация',
      headerStyle: {
        backgroundColor: '#FEDA00',
        elevation: 0,
      },
    },
  },
  RegNext: {
    screen: RegNext,
    navigationOptions: {
      title: 'Регистрация',
      headerStyle: {
        backgroundColor: '#FEDA00',
        elevation: 0,
      },
    },
  },
  Tabs: {
    screen: Tabs,
    navigationOptions: {
      header: null,
    },
  },
  FullNews: {
    screen: FullNews,
  },
  FullIvent: {
    screen: FullIvent,
  },
  MyNews: {
    screen: MyNews,
    navigationOptions: {
      title: 'Мои статьи',
      headerStyle: {
        backgroundColor: '#FEDA00',
        elevation: 0,
      },
    },
  },
  MyIvents: {
    screen: MyIvents,
    navigationOptions: {
      title: 'Мои События',
      headerStyle: {
        backgroundColor: '#FEDA00',
        elevation: 0,
      },
    },
  },
  MyJoingIvent: {
    screen: MyJoingIvent,
    navigationOptions: {
      title: 'Участвую в событиях',
      headerStyle: {
        backgroundColor: '#FEDA00',
        elevation: 0,
      },
    },
  },
  AddIvent: {
    screen: AddIvent,
    navigationOptions: {
      title: 'Добавить событие',
      headerStyle: {
        backgroundColor: '#FEDA00',
        elevation: 0,
      },
    },
  },
  AddNews: {
    screen: AddNews,
    navigationOptions: {
      title: 'Добавить новость',
      headerStyle: {
        backgroundColor: '#FEDA00',
        elevation: 0,
      },
    },
  },
  EditProfile: {
    screen: EditProfile,
    navigationOptions: {
      title: 'Редактировать',
      headerStyle: {
        backgroundColor: '#FEDA00',
        elevation: 0,
      },
    },
  },
  UserIvint: {
    screen: UserIvint,
    navigationOptions: {
      title: 'Участники',
      headerStyle: {
        backgroundColor: '#FEDA00',
        elevation: 0,
      },
    },
  },
});

export default createAppContainer(Navigate);
