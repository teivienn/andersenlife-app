import React from "react";
import { StyleSheet, Text, View, Image, Dimensions } from 'react-native';
import { createBottomTabNavigator } from "react-navigation";

import News from "../screens/News/News";
import Ivents from "../screens/Ivents/Ivents";
import Profile from "../screens/Profile/Profile";

const news = require('../assets/news.png');
const ivents = require('../assets/events.png');
const prof = require('../assets/profile.png');



// @ts-ignore
const getTabBarIcon = (navigation, tintColor) => {
  const { routeName } = navigation.state;
  if (routeName === 'News') {
    return <Image source={news} style={{tintColor: tintColor, width: 20, height: 20}} />;
  } else if (routeName === 'Ivents') {
    return <Image source={ivents} style={{tintColor: tintColor, width: 20, height: 20}} />;
  } else if(routeName === 'Profile') {
    return <Image source={prof} style={{tintColor: tintColor, width: 20, height: 20}} />;
  }

};



const Tabs = createBottomTabNavigator({
  News: News,
  Ivents: Ivents,
  Profile: Profile
},
// @ts-ignore
{
  defaultNavigationOptions: ({ navigation }) => ({
    tabBarIcon: ({focused, tintColor}) => 
    getTabBarIcon(navigation, tintColor)
  }),
  tabBarOptions: {
    activeTintColor: '#FEDA00',
    inactiveTintColor: '#C4C4C4',
    showLabel: false,
    lazy: false,
    style: {
      height: 50,
      borderTopColor: '#FEDA00',
      borderTopWidth: 0,
      elevation: 40,
    }
  }
})

export default Tabs;
