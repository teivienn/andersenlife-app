import React, { Component, PureComponent } from 'react';
import {
  StatusBar,
  StyleSheet,
  Text,
  View,
  FlatList,
  TouchableOpacity,
  Dimensions,
  Image,
  ScrollView,
  TextInput,
  ToastAndroid,
} from 'react-native';
import * as api from '../../services/api';
import IventView from '../../components/IventView';
import NewsView from '../../components/NewsView';
import ImagePicker from 'react-native-image-crop-picker';
//@ts-ignore
import DatePicker from 'react-native-datepicker';
//@ts-ignore
import Textarea from 'react-native-textarea';
import { Input, Button } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
// @ts-ignore
import AsyncStorage from '@react-native-community/async-storage';

import _url from '../../services/url';

const { width, height } = Dimensions.get('window');
const img = require('../../assets/add.png');

interface Props {
  navigation?: any;
}
interface State {
  ivents: any;
  file: any;
  datestart: any;
  dateend: any;
  description: string;
  title: string;
  value: any;
  id: any;
}
export default class AddIvent extends PureComponent<Props, State> {
  constructor(props: Props) {
    super(props);
  }

  state = {
    ivents: `${_url}/photo_chose.png`,
    file: '',
    datestart: '',
    dateend: '',
    description: '',
    title: '',
    value: '1',
    id: '',
  };

  getMyValue = async () => {
    try {
      const value = await AsyncStorage.getItem('id');
      console.log(value);
      this.setState({ id: value });
      if (value != null) {
      } else {
      }
    } catch (e) {}

    console.log('Done');
  };

  img() {
    ImagePicker.openPicker({
      width: 600,
      height: 600,
      cropping: true,
    }).then((image: any) => {
      this.setState({ ivents: image.path });

      let file = {
        uri: image.path,
        type: image.mime,
        name: image.modificationDate,
        size: image.size,
      };
      // console.log(file);

      this.setState({ file: file });
    });
  }

  componentDidMount() {
    this.getMyValue();
  }

  publish() {
    let {
      file,
      datestart,
      dateend,
      description,
      title,
      value,
      id,
    } = this.state;

    if (datestart == dateend) {
      ToastAndroid.show('одинакоые даты', 2);

      return;
    }

    if (
      file != '' &&
      datestart != '' &&
      dateend != '' &&
      description != '' &&
      title != '' &&
      value != ''
    ) {
      // console.log(file);

      let form = new FormData();
      form.append('title', title);
      form.append('description', description);
      form.append('datestart', datestart);
      form.append('dateend', dateend);
      form.append('image', file);
      form.append('count', value);
      form.append('id', id);

      api
        .addIvent(form)
        .then((data: any) => {
          console.log('sdsd', data);
          if (data == 'ok') {
            this.setState({ file: '' });
            this.setState({ ivents: `${_url}/photo_chose.png` });
            this.setState({ datestart: '' });
            this.setState({ dateend: '' });
            this.setState({ description: '' });
            this.setState({ title: '' });
            this.setState({ value: '1' });

            ToastAndroid.show('Событие опубликовано', 2);
          }
        })
        .catch(err => {
          console.log(err);
        });
    }
  }

  render() {
    let { ivents } = this.state;
    return (
      <ScrollView>
        <StatusBar backgroundColor='#FEDA00' barStyle='dark-content' />
        <View style={styles.container}>
          <Input
            value={this.state.title}
            containerStyle={styles.input}
            inputContainerStyle={styles.input_text}
            placeholder='Заголовок'
            onChangeText={title => this.setState({ title })}
          />
          <TouchableOpacity onPress={() => this.img()}>
            <Image
              style={styles.ava_image}
              source={{ uri: this.state.ivents }}
            />
          </TouchableOpacity>
          <Text>количество участников</Text>
          <TextInput
            style={styles.input_num}
            keyboardType='numeric'
            value={this.state.value}
            onChangeText={value => this.setState({ value })}
            maxLength={2}
          />
          <DatePicker
            style={styles.date_cont}
            date={this.state.datestart}
            mode='datetime'
            placeholder='Дата начала'
            format='YYYY-MM-DD, h:mm'
            confirmBtnText='Confirm'
            cancelBtnText='Cancel'
            customStyles={{
              dateIcon: {
                position: 'absolute',
                right: 0,
                top: 4,
                marginRight: -35,
              },
              dateInput: {
                borderRadius: 30,
                borderColor: '#FEDA00',
                borderWidth: 1,
              },
            }}
            onDateChange={(date: any) => {
              this.setState({ datestart: date });
            }}
          />
          <DatePicker
            style={styles.date_cont}
            date={this.state.dateend}
            mode='datetime'
            placeholder='Дата окончания'
            format='YYYY-MM-DD, h:mm'
            confirmBtnText='Confirm'
            cancelBtnText='Cancel'
            customStyles={{
              dateIcon: {
                position: 'absolute',
                right: 0,
                top: 4,
                marginRight: -35,
              },
              dateInput: {
                borderRadius: 30,
                borderColor: '#FEDA00',
                borderWidth: 1,
              },
            }}
            onDateChange={(date: any) => {
              this.setState({ dateend: date });
            }}
          />
          <Textarea
            containerStyle={styles.textareaContainer}
            style={styles.textarea}
            value={this.state.description}
            onChangeText={(value: any) => this.setState({ description: value })}
            maxLength={120}
            placeholder={'Описание...'}
            placeholderTextColor={'#c7c7c7'}
            underlineColorAndroid={'transparent'}
          />
          <TouchableOpacity
            style={styles.touch_button}
            onPress={() => this.publish()}
          >
            <LinearGradient
              colors={['#3A89C4', '#D91A25']}
              start={{ x: 0, y: 0.5 }}
              end={{ x: 1, y: 0.5 }}
              style={styles.gradient}
            >
              <Text style={styles.text_button}>Опубликовать</Text>
            </LinearGradient>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    // justifyContent: 'center',
    flex: 1,
  },
  date_cont: {
    width: 200,
    marginBottom: 10,
  },
  ava_image: {
    height: 150,
    width: 150,
    resizeMode: 'contain',
    // borderRadius: 40,
  },
  textAreaContainer: {
    // flex: 1,
    width: width * 0.9,
    margin: 10,
    borderColor: '#FEDA00',
    borderWidth: 1,
    padding: 5,
  },
  textArea: {
    height: 150,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  textareaContainer: {
    margin: 10,
    height: 100,
    width: width * 0.9,
    padding: 5,
    backgroundColor: '#fff',
    borderRadius: 20,
    borderColor: '#FEDA00',
    borderWidth: 1,
  },
  textarea: {
    textAlignVertical: 'top', // hack android
    height: 150,
    fontSize: 14,
    color: '#333',
  },
  input: {
    marginTop: 10,
    height: 40,
    borderRadius: 30,
    width: 300,
    borderColor: '#FEDA00',
    borderWidth: 1,
  },
  input_text: {
    borderBottomWidth: 0,
  },
  touch_button: {
    marginBottom: 10,
    borderRadius: 30,
    width: 300,
    height: 50,
  },
  gradient: {
    padding: 15,
    borderRadius: 30,
  },
  text_button: {
    textAlign: 'center',
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 15,
  },
  num_inpt: {
    marginBottom: 10,
  },
  input_num: {
    marginBottom: 10,
    paddingLeft: 20,
    paddingRight: 20,
    height: 40,
    borderRadius: 30,
    width: 100,
    borderColor: '#FEDA00',
    borderWidth: 1,
  },
});
