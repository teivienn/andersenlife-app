import React, { Component, PureComponent } from 'react';
import {
  StatusBar,
  StyleSheet,
  Text,
  View,
  FlatList,
  TouchableOpacity,
  Dimensions,
  Image,
  ScrollView,
  TextInput,
  ToastAndroid,
} from 'react-native';
import * as api from '../../services/api';
import Loading from '../../components/Loading';
import moment from 'moment';
import CustomButton from '../../components/CustomButton';
import AsyncStorage from '@react-native-community/async-storage';

import _url from '../../services/url';

const { width } = Dimensions.get('window');

interface Props {
  navigation?: any;
  id: number;
  title: string;
}

interface State {
  ivent: any;
  count: number | null;
  party: any;
  id: string;
  show: boolean;
  status: boolean;
  id_i: number;
}

export default class FullIvent extends PureComponent<Props, State> {
  // @ts-ignore
  static navigationOptions = ({ navigation }) => {
    let { id, title } = navigation.state.params;
    return {
      title: `${title}`,
      headerStyle: {
        backgroundColor: '#FEDA00',
        elevation: 0,
      },
    };
  };

  constructor(props: Props) {
    super(props);
  }

  state = {
    ivent: [],
    count: null,
    party: [],
    id: '',
    show: false,
    status: true,
    id_i: 0,
  };

  getMyValue = async () => {
    try {
      const id = await AsyncStorage.getItem('id');
      if (id != null) {
        this.setState({ id: id });
      } else {
      }
    } catch (e) {}
  };

  load() {
    return <Loading />;
  }

  time(date: string) {
    let now = moment();
    let time: string = moment(now).format('DD.MM.YYYY - h:mm');
    date = moment(date).format('DD.MM.YYYY - h:mm');

    if (date > time) {
      this.setState({ status: true });
      return 'идет';
    } else {
      this.setState({ status: false });

      return 'завершен';
    }
  }

  image() {
    let { ivent } = this.state;
    return `${_url}/${ivent.iimage}`;
  }

  date(date: string) {
    return moment(date).format('DD.MM.YYYY - h:mm');
  }

  party(count: number, len: number) {
    return count - len;
  }

  search(ivent: any, party: any, id: any) {
    console.log(ivent.u_id, 'dssd', Number(id));

    console.log('party', party.length);

    console.log('id', Number(id));

    for (var i = 0; i < party.length; i++) {
      console.log('i6', i);
      console.log('sds', party[i].u_id);

      if (party[i].u_id == Number(id)) {
        return (
          <View style={styles.join_cont_text}>
            <Text style={styles.status_text}>Вы уже учавствуете</Text>
          </View>
        );
      }
    }

    return (
      <View style={styles.join_cont_text}>
        <Text style={styles.status_text}>Вы не учавствуете</Text>
      </View>
    );
  }

  joinView(ivent: any, party: any, id: any) {
    // console.log(ivent.u_id, 'dssd', Number(this.state.id));

    if (this.state.status == false) {
      return (
        <View>
          <Text style={styles.status_text} />
        </View>
      );
    }

    if (ivent.u_id == Number(id)) {
      return (
        <View style={styles.join_cont_text}>
          <Text style={styles.status_text}>Вы автор</Text>
        </View>
      );
    }
    let len = party.length;
    if (len == 0) {
      console.log('len', len);
      return (
        <View style={styles.join_cont}>
          {this.state.show == false ? (
            <CustomButton
              onPress={() => this.joing()}
              leftColor='#FF3480'
              rightColor='#FEBA18'
              text='Присоедениться'
            />
          ) : (
            <Text style={styles.status_text}>Вы уже учавствуете</Text>
          )}
        </View>
      );
    } else {
      return this.search(ivent, party, id);
    }
  }

  joing() {
    let { id } = this.props.navigation.state.params;

    let { ivent, party, count, status } = this.state;

    if (status == false) {
      ToastAndroid.show('Уже закончено', 2);

      return;
    }

    let con = this.party(ivent.iusers_all, count);
    if (con == 0) {
      ToastAndroid.show('Нет мест', 2);
      return;
    }

    api.joing(id, Number(this.state.id)).then(data => {
      console.log(data);
      if (data == 'ok') {
        ToastAndroid.show('Вы приняли участие в событии', 2);
        this.setState({ show: true });
        api.partyId(id).then((data: any) => {
          // console.log('par-len -----',data.length);
          this.setState({ party: data });
          this.setState({ count: data.length });
        });
      }
    });
  }

  componentDidMount() {
    this.getMyValue();
    let { id } = this.props.navigation.state.params;
    this.setState({ id_i: id });

    api.getIventById(id).then((data: []) => {
      // console.log(data);
      this.setState({ ivent: data });
    });

    api.partyId(id).then((data: any) => {
      // console.log('par-len -----',data.length);
      this.setState({ party: data });
      this.setState({ count: data.length });
    });
  }

  render() {
    let { ivent, party, id, count, id_i } = this.state;

    // console.log('iii',ivent);

    return (
      <View style={styles.container}>
        <Image style={styles.image} source={{ uri: this.image() }} />
        <View style={styles.time_cont}>
          <View>
            <Text>начало</Text>
            <View style={styles.date_st}>
              <Text>{this.date(ivent.idatestart)}</Text>
            </View>
          </View>
          <View>
            <Text>конец</Text>
            <View style={styles.date_st}>
              <Text>{this.date(ivent.idateend)}</Text>
            </View>
          </View>
        </View>
        <View style={styles.time_cont}>
          <View>
            <Text>количество мест</Text>
            <View style={styles.col_st}>
              <Text>{ivent.iusers_all}</Text>
            </View>
          </View>
          <View>
            <Text>свободных мест</Text>
            <View style={styles.col_st}>
              <Text>{this.party(ivent.iusers_all, count)}</Text>
            </View>
          </View>
        </View>
        <View style={styles.status_cont}>
          <Text style={styles.status_text}>{this.time(ivent.idateend)}</Text>
        </View>
        <View style={styles.deck_cont}>
          <Text>Описание:</Text>
          <Text>{ivent.idiscription}</Text>
        </View>
        <View style={styles.bi_bop}>
          <CustomButton
            onPress={() =>
              this.props.navigation.navigate('UserIvint', { id_i })
            }
            text='Участники'
            leftColor='#3A89C4'
            rightColor='#D91A25'
          />
        </View>
        {this.joinView(ivent, party, id)}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  image: {
    height: 200,
    width: width,
    resizeMode: 'contain',
  },
  date_st: {
    backgroundColor: 'rgba(196, 196, 196, 0.8)',
    borderRadius: 30,
    padding: 8,
    // height: 35,
    width: 150,
    flex: 0.07,
    alignItems: 'center',
    justifyContent: 'center',
    // position: 'absolute',
    // top: width * .01,
    // right: 5
  },
  time_cont: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  col_st: {
    backgroundColor: 'rgba(196, 196, 196, 0.8)',
    borderRadius: 30,
    padding: 8,
    // height: 35,
    width: 50,
    flex: 0.07,
    alignItems: 'center',
    justifyContent: 'center',
  },
  status_cont: {
    backgroundColor: '#FEDA00',
    marginLeft: 100,
    marginRight: 100,
    marginTop: 10,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 30,
  },
  status_text: {
    color: '#000',
    fontWeight: 'bold',
    fontSize: 16,
  },
  deck_cont: {
    marginLeft: 10,
    marginRight: 10,
    marginTop: 10,
    flex: 2,
    alignItems: 'center',
  },
  join_cont: {
    alignItems: 'center',
  },
  join_cont_text: {
    alignItems: 'center',
    backgroundColor: '#FEDA00',
    marginLeft: 100,
    marginRight: 100,
    borderRadius: 30,
    marginBottom: 10,
  },
  bi_bop: {
    alignItems: 'center',
  },
});
