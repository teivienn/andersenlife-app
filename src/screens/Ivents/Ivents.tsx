import React, { Component, PureComponent } from 'react';
import {
  StatusBar,
  StyleSheet,
  Text,
  View,
  FlatList,
  TouchableOpacity,
  Dimensions, Image,
} from 'react-native';
import * as api from '../../services/api';
import IventView from "../../components/IventView";
import NewsView from "../../components/NewsView";

const {width} = Dimensions.get('window');
const img = require("../../assets/add.png");

interface Props {
  navigation?: any
 }
interface State {
  ivents: any
}
export default class Ivents extends PureComponent<Props, State> {

  constructor(props: Props) {
    super(props)
  }

  state = {
    ivents: []
  };



  async componentDidMount(){
    try {
      api.allIvent(100, 0)
      .then((data: any) => {
        // console.log('ivent up');
        
        this.setState({ivents: data})
      });
       setInterval(()=> {
        api.allIvent(100, 0)
        .then((data: any) => {
          // console.log('ivent up');
          
          this.setState({ivents: data})
        });
      }, 5000);


    } catch (e) {
      console.log(e)
    }

  };

  render() {
    let {ivents} = this.state;
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor="#FEDA00" barStyle="dark-content" />
        <FlatList
          data={ivents}
          renderItem={({item}: {item: any}) => {
           return(
             <IventView
               openNews={(id : number, title: string) => this.props.navigation.navigate('FullIvent', {id, title})}
               date={item.idatestart}
               image={item.iimage}
               description={item.idiscription}
               title={item.ititile}
               user={item.iusers_all}
               id={item.iid}
             />
           )
          }}
        />
        <TouchableOpacity
          // @ts-ignore
          onPress={() => this.props.navigation.navigate('AddIvent')}
          style={styles.add_container}>
          <Image
            style={styles.add_image}
            source={img}/>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: width * 1.57
  },
  add_container: {
    // backgroundColor: '#FEDA00',
    borderRadius: 30,
    position: 'absolute',
    bottom: width * .05,
    right: 20,
    height: 55,
    width: 55,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowOpacity: 1,
    shadowRadius: 1.41,
    elevation: 8
  },
  add_image: {
    height: 55,
    width: 55,
  }
});