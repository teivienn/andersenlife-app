import React, { Component, PureComponent } from 'react';
import {
  StatusBar,
  StyleSheet,
  Text,
  View,
  FlatList,
  TouchableOpacity,
  Dimensions,
  Image,
} from 'react-native';
import * as api from '../../services/api';
import IventView from '../../components/IventView';
import NewsView from '../../components/NewsView';
import _url from '../../services/url';

const { width, height } = Dimensions.get('window');
const img = require('../../assets/add.png');

interface Props {
  navigation?: any;
}
interface State {
  ivents: any;
}
export default class UserIvents extends PureComponent<Props, State> {
  constructor(props: Props) {
    super(props);
  }

  state = {
    ivents: [],
  };

  async componentDidMount() {
    let { id_i } = this.props.navigation.state.params;
    console.log(id_i);

    try {
      api.getIventUserById(id_i).then((data: any) => {
        console.log(data);

        this.setState({ ivents: data });
      });
    } catch (e) {
      console.log(e);
    }
  }

  nocont() {
    return (
      <View style={styles.no_cont}>
        <Text style={styles.no_c_text}>Никто не участвует</Text>
      </View>
    );
  }

  static viewPhoto(image: string) {
    return `${_url}/${image}`;
  }

  static ava(image: string): any {
    if (image == null) {
      return this.viewPhoto('defalt_user.png');
    } else {
      return this.viewPhoto(image);
    }
  }

  list(ivents: any) {
    return (
      <FlatList
        data={ivents}
        renderItem={({ item }: { item: any }) => {
          return (
            <View style={styles.user_cont}>
              <Image
                style={styles.u_image}
                source={{ uri: UserIvents.ava(item.photo) }}
              />
              <Text style={styles.u_text}>
                {item.uname} {item.usurname}
              </Text>
            </View>
          );
        }}
      />
    );
  }

  render() {
    let { ivents } = this.state;
    console.log(ivents);
    return (
      <View>
        <StatusBar backgroundColor='#FEDA00' barStyle='dark-content' />
        {ivents.length == 0 ? this.nocont() : this.list(ivents)}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  add_container: {
    // backgroundColor: '#FEDA00',
    borderRadius: 30,
    position: 'absolute',
    bottom: width * 0.05,
    right: 20,
    height: 55,
    width: 55,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 1,
    shadowRadius: 1.41,
    elevation: 8,
  },
  add_image: {
    height: 55,
    width: 55,
  },
  no_cont: {
    height: height,
    alignItems: 'center',
    justifyContent: 'center',
  },
  no_c_text: {
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 22,
    color: '#FEDA00',
  },
  user_cont: {
    padding: 5,
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomColor: '#000',
    borderBottomWidth: 0.3,
  },
  u_image: {
    width: width * 0.2,
    height: width * 0.2,
    borderRadius: 100,
  },
  u_text: {
    paddingLeft: 20,
    fontSize: 18,
    color: '#000',
  },
});
