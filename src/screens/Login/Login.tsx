import React, { Component, PureComponent } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  StatusBar,
  TextInput,
  TouchableOpacity,
  TouchableHighlightBase,
  AlertAndroid,
  ToastAndroid
} from 'react-native';
// @ts-ignore
import AsyncStorage from '@react-native-community/async-storage';
import {inspect} from "util";
import { Input, Button } from 'react-native-elements'; 
// @ts-ignore
import Icon from 'react-native-vector-icons/FontAwesome';
import LinearGradient from 'react-native-linear-gradient';
import * as api from '../../services/api';
import { NavigationActions, StackActions } from 'react-navigation';

const tabs = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: 'Tabs' })],
})



const { width } = Dimensions.get('window');

interface IProps {
  navigation?:any
}
interface State {
  login: string,
  password: string
}
export default class Login extends PureComponent<IProps, State> {

  constructor(props:IProps) {
    super(props);
  }

  state = {
    login: '',
    password: ''
  }

  checkUser(data: any) {
    
    const {login, password} = this.state;
    if(login == data.umail && password == data.upass) {
      AsyncStorage.setItem('id', String(data.uid));      
      AsyncStorage.setItem('email', data.umail);
      AsyncStorage.setItem('photo', data.photo);
      AsyncStorage.setItem('password', data.upass);
      AsyncStorage.setItem('surname', data.usurname);
      AsyncStorage.setItem('name', data.uname);
      this.props.navigation.dispatch(tabs);
      console.log('ok');
      
    } else {
      console.log('eerr');

    }
  }

  test() {
   
  }

  login() {
    let {login, password} = this.state;
    api.Login(login, password)
    .then((data: any) => {
      console.log(data.uid);
      
      if(data[0] == 'no user') {
        ToastAndroid.show('Пользователь не найден', 2)
      } else {
        this.checkUser(data)
      }
      
    })
  }


  render() {
    let {login, password} = this.state;
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor="#FEDA00" barStyle="dark-content" />
        <View style={styles.form}>
          <Input
            containerStyle={styles.input}
            inputContainerStyle={styles.input_text}
            placeholder='E-mail'
            onChangeText={(login) => this.setState({login})}
            value={login}
            leftIcon={
              <Icon
                name='user'
                size={24}
                color='#FEDA00'
              />
            }
          />

          <Input
            containerStyle={styles.input}
            inputContainerStyle={styles.input_text}
            placeholder='Пароль'
            onChangeText={(password) => this.setState({password})}
            value={password}
            secureTextEntry={true}
            leftIcon={
              <Icon
                name='lock'
                size={24}
                color='#FEDA00'
              />
            }
            shake={true}
          />
        </View>
          <View style={styles.button_cont}>
            <TouchableOpacity
              style={styles.touch_button}
              onPress={() => this.login()}>
              <LinearGradient
                colors={['#FF3480', '#FEBA18']}
                start={{x: 0, y: 0.5}}
                end={{x: 1, y: 0.5}}
                style={styles.gradient}>
                <Text style={styles.text_button}>
                  Войти
                </Text>
              </LinearGradient>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.touch_button}
              onPress={() => this.props.navigation.navigate('Reg')}>
              <LinearGradient
                colors={['#3A89C4', '#D91A25']}
                start={{x: 0, y: 0.5}}
                end={{x: 1, y: 0.5}}
                style={styles.gradient}>
                <Text style={styles.text_button}>
                  Регистрация
                </Text>
              </LinearGradient>
            </TouchableOpacity>
          </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  form: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'center'
  },
  input: {
    marginBottom: 10,
    height: 50,
    borderRadius: 30,
    width: 300,
    borderColor: '#FEDA00',
    borderWidth: 1,
  },
  input_text: {
    borderBottomWidth: 0
  },
  button: {
    borderRadius: 30,
    width: 300,
    height: 50,
  },
  touch_button: {
    marginBottom: 10,
    borderRadius: 30,
    width: 300,
    height: 50,
  },
  gradient: {
    padding: 15,
    borderRadius: 30
  },
  text_button: {
    textAlign: 'center',
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 15,
  },
  button_cont: {
    marginTop: 10,
    flex: .51,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'center'
  }
});