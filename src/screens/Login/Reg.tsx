import React, { Component, PureComponent } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  StatusBar,
  TextInput,
  TouchableOpacity,
  ToastAndroid
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {inspect} from "util";
// import AnimatedForm from 'react-native-animated-form';
import { Input, Button } from 'react-native-elements';
// @ts-ignore
import Icon from 'react-native-vector-icons/FontAwesome';
import LinearGradient from 'react-native-linear-gradient';
const { width } = Dimensions.get('window');

interface IProps {
  navigation?:any
}
interface State {
  name: string,
  surname: string,
  onPress?: any
}
export default class Reg extends PureComponent<IProps, State> {

  constructor(props:IProps) {
    super(props);
  }

  state = {
    name: '',
    surname: ''
  }

  Validation() {
    const {name, surname} = this.state;

    if(name == '' || surname == '') {
      ToastAndroid.show('Поля не могут быть пусты!', 2)
    } else {
      this.props.navigation.navigate('RegNext', {name: name, surname: surname})
    }
  }


  render() {

    let {name, surname} = this.state;

    return (
      <View style={styles.container}>
        <StatusBar backgroundColor="#FEDA00" barStyle="dark-content" />
        <View style={styles.progress}></View>
        <View style={styles.form}>
          <Input
            containerStyle={styles.input}
            inputContainerStyle={styles.input_text}
            placeholder='Имя'
            onChangeText={(name) => this.setState({name})}
            value={name}
          />

          <Input
            containerStyle={styles.input}
            inputContainerStyle={styles.input_text}
            placeholder='Фамилия'
            onChangeText={(surname) => this.setState({surname})}
            value={surname}
          />
        </View>
        <View style={styles.button_cont}>
          <TouchableOpacity
            style={styles.touch_button}
            onPress={() => this.Validation()}>
            <LinearGradient
              colors={['#3A89C4', '#D91A25']}
              start={{x: 0, y: 0.5}}
              end={{x: 1, y: 0.5}}
              style={styles.gradient}>
              <Text style={styles.text_button}>
                Next
              </Text>
            </LinearGradient>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  progress: {
    height: 3,
    width: width * .5,
    backgroundColor: '#3A89C4',
  },
  form: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'center'
  },
  input: {
    marginBottom: 10,
    height: 50,
    borderRadius: 30,
    width: 300,
    borderColor: '#FEDA00',
    borderWidth: 1,
  },
  input_text: {
    borderBottomWidth: 0
  },
  button: {
    borderRadius: 30,
    width: 300,
    height: 50,
  },
  touch_button: {
    marginBottom: 10,
    borderRadius: 30,
    width: 300,
    height: 50,
  },
  gradient: {
    padding: 15,
    borderRadius: 30
  },
  text_button: {
    textAlign: 'center',
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 15,
  },
  button_cont: {
    marginTop: 10,
    flex: .51,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'center'
  }
});