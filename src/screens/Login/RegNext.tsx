import React, { Component, PureComponent } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  StatusBar,
  TextInput,
  TouchableOpacity,
  ToastAndroid,
  Alert
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {inspect} from "util";
import { Input, Button } from 'react-native-elements';
// @ts-ignore
import Icon from 'react-native-vector-icons/FontAwesome';
import LinearGradient from 'react-native-linear-gradient';
import * as api from '../../services/api';
import { NavigationActions, StackActions } from 'react-navigation';

const tabs = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: 'Login' })],
})




const { width } = Dimensions.get('window');

interface IProps {
  navigation?:any,
  name: string,
  surname: string,
}
interface State {
  email: string,
  password: string,
  pass_check: string
}
export default class RegNext extends PureComponent<IProps, State> {

  constructor(props:IProps) {
    super(props);

  }

  state = {
    email: '',
    password: '',
    pass_check: ''
  }


  reg() {
    let {name, surname} = this.props.navigation.state.params
    let {email, password} = this.state;
    api.registration(name, surname, password, email)
    .then((data: any) => {
      // console.log(data[0]);
      if(data[0] == 'user exists') {
        ToastAndroid.show('E-mail занят другим пользователем!', 5)
      } 
      if(data[0] == 'ok') {
        ToastAndroid.show('Ваш аккаунт успешно зарегестрирован', 10)
        setTimeout(()=> {
          this.props.navigation.dispatch(tabs);
        }, 2000)
      }
      
    })
  }

  check() {
    let {email, password, pass_check} = this.state;

    if(email == '' || password == '' || pass_check == '') {
      ToastAndroid.show('Поля не могут быть пусты!', 2)
    } else {

      if(password != pass_check) {
        ToastAndroid.show('Пароли не совпадают!', 2)
      } else {
        this.reg()
      }

    }

  }


  render() {
    let {email, password, pass_check} = this.state;
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor="#FEDA00" barStyle="dark-content" />
        <View style={styles.progress}></View>
        <View style={styles.form}>
          <Input
            containerStyle={styles.input}
            inputContainerStyle={styles.input_text}
            placeholder='E-mail'
            value={email}
            onChangeText={(email) => this.setState({email})}
          />

          <Input
            containerStyle={styles.input}
            inputContainerStyle={styles.input_text}
            placeholder='Пароль'
            secureTextEntry={true}
            value={password}
            onChangeText={(password) => this.setState({password})}
          />

          <Input
            containerStyle={styles.input}
            inputContainerStyle={styles.input_text}
            placeholder='Повторите пароль'
            secureTextEntry={true}
            value={pass_check}
            onChangeText={(pass_check) => this.setState({pass_check})}            
          />

        </View>
        <View style={styles.button_cont}>
          <TouchableOpacity
            style={styles.touch_button}
            onPress={() => this.check()}>
            <LinearGradient
              colors={['#FF3480', '#FEBA18']}
              start={{x: 0, y: 0.5}}
              end={{x: 1, y: 0.5}}
              style={styles.gradient}>
              <Text style={styles.text_button}>
                Продолжить
              </Text>
            </LinearGradient>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  progress: {
    height: 3,
    width: width,
    backgroundColor: '#3A89C4',
  },
  form: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'center'
  },
  input: {
    marginBottom: 10,
    height: 50,
    borderRadius: 30,
    width: 300,
    borderColor: '#FEDA00',
    borderWidth: 1,
  },
  input_text: {
    borderBottomWidth: 0
  },
  button: {
    borderRadius: 30,
    width: 300,
    height: 50,
  },
  touch_button: {
    marginBottom: 10,
    borderRadius: 30,
    width: 300,
    height: 50,
  },
  gradient: {
    padding: 15,
    borderRadius: 30
  },
  text_button: {
    textAlign: 'center',
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 15,
  },
  button_cont: {
    marginTop: 10,
    flex: .51,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'center'
  }
});