import React, { Component, PureComponent } from 'react';
import { View, Text } from 'react-native';
import { WebView } from 'react-native-webview';
import Loading from '../../components/Loading'
import AsyncStorage from '@react-native-community/async-storage';



interface Props {}
export default class AddNews extends PureComponent<Props> {

  constructor(props: Props) {
    super(props);

  }

  state = {
    id: ''
  }

  getMyValue = async () => {
    try {
      const value = await AsyncStorage.getItem('id')
      console.log(value);
      if(value != null) {
          
          this.setState({id: value})
      } else {

      }
    } catch(e) {
    }
  
  }

  getAllKeys = async () => {
    let keys: any = []
    try {
      keys = await AsyncStorage.getAllKeys()
    } catch(e) {
      // read key error
    }
  
    console.log(keys)
    // example console.log result:
    // ['@MyApp_user', '@MyApp_key']
  }

  componentDidMount() {
    this.getMyValue()
  }

  render() {
    console.log(this.state.id);
    
    return (
      <WebView 
        scalesPageToFit = { false }    
        startInLoadingState={true}
        renderLoading={() => <Loading />}  
        source={{ uri: `http://192.168.43.70:8080/${this.state.id}` }} />
    )
  }
}
