import React, { Component, PureComponent } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  StatusBar,
  TouchableOpacity
} from 'react-native';
import { WebView } from 'react-native-webview';
const { width } = Dimensions.get('window');
import * as api from '../../services/api';
import Loading from '../../components/Loading'

interface Props {
  id: number,
  navigation?: any
}

interface state {
  html: string
}

export default class FullNews extends PureComponent<Props> {

  // @ts-ignore
  static navigationOptions = ({ navigation }) => {
    let {id, title} = navigation.state.params;
    return {
      title: `${title}`,
      headerStyle: {
        backgroundColor: '#FEDA00',
        elevation: 0,
      },

    }
  };


  constructor(props: Props) {
    super(props)
  }

  state = {
    html: ''
  }

  componentDidMount() {
    let {id} = this.props.navigation.state.params;
    api.getNewsById(id)
    .then((data: any) => {
      console.log(data[0]);
      
      this.setState({url: data[0].ntemplate})
    })
  }

  render() {
    let {id} = this.props.navigation.state.params;

    console.log(this.state.html);
    return(
      <WebView 
        scalesPageToFit = { false }
        originWhitelist = {[ ' * ' ]}
        startInLoadingState={true}
        renderLoading={() => <Loading />}
        source={{ uri: `http://192.168.43.70:8080/article/${id}` }} />
    )
  }

};