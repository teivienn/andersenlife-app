import React, { Component, PureComponent } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  FlatList,
  StatusBar,
  TouchableOpacity,
  Image,
} from 'react-native';
import * as api from '../../services/api';
import NewsView from '../../components/NewsView';

const { width } = Dimensions.get('window');
const img = require('../../assets/add.png');

interface Props {
  navigation?: any;
  news: any;
}
interface State {
  news: any;
}
export default class News extends PureComponent<Props, State> {
  static navigationOptions = { header: null };

  constructor(props: Props) {
    super(props);
  }

  state = {
    news: [],
  };

  async componentDidMount() {
    try {
      api.allNews(500, 0).then((data: any) => {
        console.log(data);

        this.setState({ news: data });
      });
      setInterval(() => {
        api.allNews(50, 0).then((data: any) => {
          // console.log('news up');

          this.setState({ news: data });
        });
      }, 5000);
    } catch (e) {
      console.log(e);
    }
  }

  render() {
    let { news } = this.state;
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor='#FEDA00' barStyle='dark-content' />
        <FlatList
          data={news}
          renderItem={({ item }: { item: any }) => {
            return (
              <NewsView
                openNews={(id: any, title: any) =>
                  this.props.navigation.navigate('FullNews', { id, title })
                }
                date={item.ndatecreate}
                image={item.nphoto}
                description={item.ndiscription}
                title={item.ntitle}
                id={item.nid}
                uava={item.photo}
                uname={item.uname}
                usurname={item.usurname}
              />
            );
          }}
        />
        <TouchableOpacity
          onPress={() => this.props.navigation.navigate('AddNews')}
          style={styles.add_container}
        >
          <Image style={styles.add_image} source={img} />
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: width * 1.57,
  },
  add_container: {
    // backgroundColor: '#FEDA00',
    borderRadius: 30,
    position: 'absolute',
    bottom: width * 0.05,
    right: 20,
    height: 55,
    width: 55,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 1,
    shadowRadius: 1.41,
    elevation: 8,
  },
  add_image: {
    height: 55,
    width: 55,
  },
});
