import React, { Component, PureComponent } from 'react';
import { 
  StyleSheet,
  Text, 
  View, 
  Alert,
  Image,
  TouchableOpacity,
  Dimensions,
  ScrollView,
  ToastAndroid
} from 'react-native';
import CustomButton from "../../components/CustomButton";
import { Input, Button } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import * as api from '../../services/api';
import AsyncStorage from '@react-native-community/async-storage';


interface Props {
  navigation?: any,
  id: any,
  name: string,
  surname: string,
  email: string,
  password: string,
  photo: any
}
interface State {
  names: string,
  surnames: string,
  emails: string,
  passwords: string,
}


export default class EditProfile extends PureComponent<Props, State> {

  constructor(props: Props) {
    super(props)
  }

  state ={ 
    names: '',
    surnames: '',
    emails: '',
    passwords: '',
  }

  change() {
    let {id, name, surname, email, password, photo} = this.props.navigation.state.params;
    let {names, surnames, emails, passwords} = this.state;

    if(names != '' && surnames != '' && emails != '' && passwords != '') {
      let from = new FormData();
      from.append('id', id);
      from.append('name', names);
      from.append('surname', surnames);
      from.append('password', passwords);
      from.append('email', emails);


      api.UserChange(from)
      .then(data => {
        console.log(data);
        
        if(data = 'ok') {
         ToastAndroid.show('Изменено', 2)
         AsyncStorage.setItem('name', names);
         AsyncStorage.setItem('surname', surnames);
         AsyncStorage.setItem('password', passwords);
         AsyncStorage.setItem('email', emails);


        }
      })

    } else {
      ToastAndroid.show('Заполните данные', 2)

    }

  }

  componentDidMount() {
    let {id, name, surname, email, password, photo} = this.props.navigation.state.params;
    console.log(id, name, surname, email, password, photo);
    // this.setState({photo: 'image.jpeg'})
  }

  render() {
    let {names, surnames, emails, passwords} = this.state;
    return(
      <View style={styles.container}>
        <Input
          containerStyle={styles.input}
          inputContainerStyle={styles.input_text}
          placeholder='E-mail'
          onChangeText={(emails) => this.setState({emails})}
          value={emails}
          />
        <Input
          containerStyle={styles.input}
          inputContainerStyle={styles.input_text}
          placeholder='Имя'
          onChangeText={(names) => this.setState({names})}
          value={names}
          />
        <Input
          containerStyle={styles.input}
          inputContainerStyle={styles.input_text}
          placeholder='Фамилия'
          onChangeText={(surnames) => this.setState({surnames})}
          value={surnames}
          />
        <Input
          containerStyle={styles.input}
          inputContainerStyle={styles.input_text}
          placeholder='Пароль'
          secureTextEntry={true}
          onChangeText={(passwords) => this.setState({passwords})}
          value={passwords}
          />
        <TouchableOpacity
          style={styles.touch_button}
          onPress={() => this.change()}>
          <LinearGradient
            colors={['#3A89C4', '#D91A25']}
            start={{x: 0, y: 0.5}}
            end={{x: 1, y: 0.5}}
            style={styles.gradient}>
            <Text style={styles.text_button}>
              Изменить
            </Text>
          </LinearGradient>
        </TouchableOpacity>
      </View>
    )
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'center'
  },
  form: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'center'
  },
  input: {
    marginBottom: 10,
    height: 50,
    borderRadius: 30,
    width: 300,
    borderColor: '#FEDA00',
    borderWidth: 1,
  },
  input_text: {
    borderBottomWidth: 0
  },
  button: {
    borderRadius: 30,
    width: 300,
    height: 50,
  },
  touch_button: {
    marginBottom: 10,
    borderRadius: 30,
    width: 300,
    height: 50,
  },
  gradient: {
    padding: 15,
    borderRadius: 30
  },
  text_button: {
    textAlign: 'center',
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 15,
  },
  button_cont: {
    marginTop: 10,
    flex: .51,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'center'
  }
});