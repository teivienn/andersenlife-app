import React, { Component, PureComponent } from 'react';
import {
  StatusBar,
  StyleSheet,
  Text,
  View,
  FlatList,
  TouchableOpacity,
  Dimensions, Image,
} from 'react-native';
import * as api from '../../services/api';
import IventView from "../../components/IventView";
import NewsView from "../../components/NewsView";

const {width, height} = Dimensions.get('window');
const img = require("../../assets/add.png");

interface Props {
  navigation?: any
 }
interface State {
  ivents: any
}
export default class MyJoingIvent extends PureComponent<Props, State> {

  constructor(props: Props) {
    super(props)
  }

  state = {
    ivents: []
  };



  async componentDidMount(){
    let {id} = this.props.navigation.state.params;

    try {
      api.myjoIvent(id)
        .then((data: any) => {

          this.setState({ivents: data})
        });
    } catch (e) {
      console.log(e)
    }

  }


  nocont() {
    return(
      <View style={styles.no_cont}>
        <Text style={styles.no_c_text}>Вы не участвуете ни в одном событии</Text>
      </View>
    )
  }

  list(ivents: any) {
    return(
      <FlatList
      data={ivents}
      renderItem={({item}: {item: any}) => {
       return(
         <IventView
           openNews={(id : number, title: string) => this.props.navigation.navigate('FullNews', {id, title})}
           date={item.idatestart}
           image={item.iimage}
           description={item.idiscription}
           title={item.ititile}
           user={item.iusers_all}
           id={item.iid}
         />
       )
      }}
    />
    )
  }

  render() {
    let {ivents} = this.state;
    console.log(ivents);
    return (
      <View>
        <StatusBar backgroundColor="#FEDA00" barStyle="dark-content" />
        { 
          ivents.length == 0 ? this.nocont() : this.list(ivents)
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  add_container: {
    // backgroundColor: '#FEDA00',
    borderRadius: 30,
    position: 'absolute',
    bottom: width * .05,
    right: 20,
    height: 55,
    width: 55,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowOpacity: 1,
    shadowRadius: 1.41,
    elevation: 8
  },
  add_image: {
    height: 55,
    width: 55,
  },
  no_cont: {
    height: height,
    alignItems: 'center',
    justifyContent: 'center',
  },
  no_c_text: {
    textAlign:'center',    
    fontWeight: 'bold',
    fontSize: 22,
    color: '#FEDA00',
  }
});