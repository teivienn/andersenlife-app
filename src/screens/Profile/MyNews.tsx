import React, { Component, PureComponent } from 'react';
import { 
  StyleSheet,
  Text, 
  View, 
  Alert,
  Image,
  TouchableOpacity,
  Dimensions,
  ScrollView,
  StatusBar,
  FlatList
} from 'react-native';
import MyNewsView from "../../components/MyNewsView";
import * as api from '../../services/api';
import Loading from '../../components/Loading';

const {width, height} = Dimensions.get('window');


interface Props {
  navigation?: any,
  id: any,
}
interface State {
  news: any,
}
export default class MyNews extends PureComponent<Props, State> {

  constructor(props: Props) {
    super(props)
  }

  state = {
    news: []
  }

  async componentDidMount(){
    let {id} = this.props.navigation.state.params;
    console.log(id);
    
    try {
      api.myNews(id)
        .then((data: any) => {
          console.log(data);
          
          this.setState({news: data})
        });
    } catch (e) {
     console.log(e)
    }

  };

  load() {
    return(
      <Loading />
    )
  }

  nocont() {
    return(
      <View style={styles.no_cont}>
        <Text style={styles.no_c_text}>Вы не разместили ни одной статьи</Text>
      </View>
    )
  }

  list(news: any) {



    return(
      <FlatList
      data={news}
      renderItem={({item}: {item:any}) => {
        return(
          <MyNewsView
            openNews={(id: any, title:any) => this.props.navigation.navigate('FullNews', {id, title})}
            date={item.ndatecreate}
            image={item.nphoto}
            description={item.ndiscription}
            title={item.ntitle}
            id={item.nid}
          />
          ) 
        }}
      />
    )
  }



  render() {
    let {news} = this.state;
    console.log(news);
    
    return(
      <View style={styles.container}>
        <StatusBar backgroundColor="#FEDA00" barStyle="dark-content" />        
        { 
          news.length == 0 ? this.nocont() : this.list(news)
        }
      </View>
    )
  }
}



const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  add_container: {
    // backgroundColor: '#FEDA00',
    borderRadius: 30,
    position: 'absolute',
    bottom: width * .05,
    right: 20,
    height: 55,
    width: 55,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowOpacity: 1,
    shadowRadius: 1.41,
    elevation: 8
  },
  add_image: {
    height: 55,
    width: 55,
  },
  no_cont: {
    height: height,
    alignItems: 'center',
    justifyContent: 'center',
  },
  no_c_text: {
    textAlign:'center',    
    fontWeight: 'bold',
    fontSize: 22,
    color: '#FEDA00',
  },
  loading: {

    height: height
  }
});
