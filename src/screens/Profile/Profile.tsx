import React, { Component, PureComponent } from 'react';
import { 
  StyleSheet,
  Text, 
  View, 
  Alert,
  Image,
  TouchableOpacity,
  Dimensions,
  ScrollView
} from 'react-native';
import CustomButton from "../../components/CustomButton";
import * as api from '../../services/api';
import { object } from 'prop-types';
import AsyncStorage from '@react-native-community/async-storage';
import _url from '../../services/url'
import ImagePicker from 'react-native-image-crop-picker';


const {width} = Dimensions.get('window');
const img = require("../../assets/pencil.png")
const ava = require("../../assets/avatar.png")

interface Props { 
  navigation?:any
}
interface State {
  user: any,
  id: any,
  name: string,
  surname: string,
  email: string,
  password: string,
  photo: any,
  file: any
}
export default class Ivents extends PureComponent<Props, State> {
  refreshIntervalId: any;
  constructor(props:Props) {
    super(props)

  }
  state = {
    user: [],
    id: '',
    name: '',
    surname: '',
    email: '',
    password: '',
    photo: `${_url}/defalt_user.png`,
    file: []
  };

  getMyValue = async () => {
    try {
      const id = await AsyncStorage.getItem('id')
      const name = await AsyncStorage.getItem('name')
      const surname = await AsyncStorage.getItem('surname')
      const email = await AsyncStorage.getItem('email')
      const password = await AsyncStorage.getItem('password')
      const photo = await AsyncStorage.getItem('photo')



      // console.log(id, name, surname, email, password, photo);
      if(id != null && name != null && surname != null && email != null && password != null) {
        
        this.setState({id: id})
        this.setState({name: name})
        this.setState({surname: surname})
        this.setState({email: email})
        this.setState({password: password})
        if(photo != null) {
          this.setState({photo: `${_url}/${photo}`})
        }


      } else {

      }
    } catch(e) {
    }
  }

  img() {
    ImagePicker.openPicker({
      width: 600,
      height: 600,
      cropping: true
    }).then((image: any) => {
      console.log(image.modificationDate);
      
      this.setState({photo: image.path})
      AsyncStorage.setItem('photo', image.modificationDate);
      let file = {
        uri: image.path,
        type: image.mime,
        name: image.modificationDate,
        size: image.size,
      }
      // console.log(file);
      

      let from = new FormData();
      from.append('id', this.state.id)
      from.append('image', file)
      api.UserPhoto(from)
      .then(data => {
        console.log(data);
        
      })
      this.setState({file: file})

    });
  }

  clearAll = async () => {
    try {
      await AsyncStorage.clear()
      this.props.navigation.navigate('Splash')
    } catch(e) {
      // clear error
    }
  
    console.log('clear.')
  }

  exit() {
    Alert.alert(
      'Выйти?',
      '',
      [
        {
          text: 'Нет',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {text: 'Да', onPress: () => this.clearAll()},
      ],
      {cancelable: false},
    );
  }

  getUser() {
    api.getUserBuId(2)
    .then((data: any) => {
      console.log(data[0]);
      this.setState({user: data[0]})
      
    })
  }


  viewPhoto() {

    let {photo} = this.state;


    if(photo == null) {
    return `${_url}/defalt_user.png`
      
    }

    return `${_url}/${photo}`
  }


  componentDidMount() {
    this.getMyValue()

    console.log('did');
    setInterval(()=> {
      this.getMyValue()
    }, 5000);

  }
  render() {

    const {id, name, surname, email, password, photo} = this.state;
    
    return (
      <ScrollView>
        <View style={styles.container}>
          <TouchableOpacity
            style={styles.edit_button}
            onPress={() => this.props.navigation.navigate('EditProfile', {id, name, surname, email, password, photo})}
            >
            <Image
              style={styles.edit_image}
              source={img}/>
          </TouchableOpacity>

          <View style={styles.ava_container}>
          <TouchableOpacity
            onPress={() => this.img()}>
              <Image style={styles.ava_image} source={{uri: photo}}/>
          </TouchableOpacity>
          </View>
          <Text
            style={styles.avtor}
            >{ name}  { surname}</Text>
          <CustomButton 
            onPress={()=> this.props.navigation.navigate('MyJoingIvent', {id})}
            leftColor="#FF3480"
            rightColor="#FEBA18"
            text="Участвую в событиях"
          />
          <CustomButton 
            onPress={()=> this.props.navigation.navigate('MyIvents', {id})}
            leftColor="#FF3480"
            rightColor="#FEBA18"
            text="Мои ивенты"
          />
          <CustomButton 
            onPress={()=> this.props.navigation.navigate('MyNews', {id})}
            leftColor="#FF3480"
            rightColor="#FEBA18"
            text="Мои статьи"
          />

          <CustomButton 
            onPress={()=> this.exit()}
            leftColor="#C4C4C4"
            rightColor="#C4C4C4"
            text="Выход"
          />
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: '#F5FCFF',
  },
  edit_button: {
    borderRadius: 30,
    position: 'absolute',
    top: width * .05,
    right: 20,
    height: 55,
    width: 55,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowOpacity: 1,
    shadowRadius: 1.41,
    elevation: 4
  },
  edit_image: {
    height: 55,
    width: 55,
  },
  ava_container: {
    marginTop: 20,
  },
  ava_image: {
    height: 200,
    width: 200,
    borderRadius: 100,
  },
  avtor: {
    paddingTop: 20,
    paddingBottom: 20,
    textAlign: 'center',
    color: '#000',
    fontWeight: 'bold',
    fontSize: 20,
  },
  exit_button: {
    width: width * .5,
  }
});