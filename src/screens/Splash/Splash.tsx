import React, { Component, PureComponent } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  StatusBar,
  ToastAndroid,
} from 'react-native';
import SVGImage from '../../components/SVGImage';
import { BarIndicator } from 'react-native-indicators';
const { width } = Dimensions.get('window');
const img = require('../../assets/logo.svg');
import { NavigationActions, StackActions } from 'react-navigation';
import AsyncStorage from '@react-native-community/async-storage';
import * as api from '../../services/api';

const login = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: 'Login' })],
});

const tabs = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: 'Tabs' })],
});

interface Props {
  navigation?: any;
}
export default class Splash extends PureComponent<Props> {
  static navigationOptions = { header: null };

  constructor(props: Props) {
    super(props);
    this.state = {
      continue: false,
    };
  }

  getMyValue = async () => {
    try {
      const value = await AsyncStorage.getItem('email');
      console.log(value);

      if (value != null) {
        this.props.navigation.dispatch(tabs);
      } else {
        this.props.navigation.dispatch(login);
      }
    } catch (e) {}

    console.log('Done');
  };

  componentDidMount() {
    setTimeout(() => {
      api
        .connect()
        .then(data => {
          console.log(data);

          if (data[0] == 'andersenLife') {
            setTimeout(() => {
              this.getMyValue();
            }, 1000);
          }
        })
        .catch(err => {
          ToastAndroid.show('Нет соединения с сервером', 2);
        });
    }, 10000);
  }

  render() {
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor='#FEDA00' barStyle='dark-content' />
        <View style={styles.logo}>
          <SVGImage height={100} width={300} source={img} />
        </View>
        <BarIndicator color='#FEDA00' />
        <View style={styles.buttomPanel} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  logo: {
    // backgroundColor: '#FEDAF0',
    flexDirection: 'column',
    justifyContent: 'flex-end',
    alignItems: 'center',
    height: width * 0.9,
  },
  buttomPanel: {
    backgroundColor: '#FEDA00',
    height: width * 0.05,
  },
});
