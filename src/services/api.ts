import _url from './url';

export function registration(
  name: string,
  surname: string,
  password: string,
  email: string,
) {
  let data = new FormData();
  data.append('name', name);
  data.append('surname', surname);
  data.append('email', email);
  data.append('password', password);

  return fetch(`${_url}/user/reg`, {
    method: 'post',
    body: data,
  }).then(res => res.json());
}

export function getUserBuId(id: number) {
  return fetch(`${_url}/user/get/${id}`, {
    method: 'get',
  }).then(res => res.json());
}

export function Login(email: string, password: string) {
  let data = new FormData();
  data.append('email', email);
  data.append('password', password);

  return fetch(`${_url}/user/login`, {
    method: 'post',
    body: data,
  }).then(res => res.json());
}

export function allNews(start: number, offset: number) {
  return select(`news/all/${start}/${offset}`);
}

export function getNewsById(id: number) {
  return select(`news/get/${id}`);
}

export function allIvent(start: number, offset: number) {
  return select(`ivents/all/${start}/${offset}`);
}

export function myjoIvent(id: number) {
  return select(`user/joing/ivent/${id}`);
}

export function myNews(id: number) {
  return select(`user/news/${id}`);
}

export function myIvent(id: number) {
  return select(`user/ivent/${id}`);
}

export function addIvent(data: any) {
  return fetch(`${_url}/ivents/add`, {
    method: 'post',
    body: data,
  })
    .then(res => res.json())
    .catch(er => {
      console.log('error', er);
    })
    .then(data => {
      return data;
    });
}

export function getIventById(id: number) {
  return select(`ivents/get/${id}`);
}

export function partyId(id: number) {
  return select(`ivents/party/${id}`);
}

export function joing(i_id: number, u_id: number) {
  return select(`ivents/join/${i_id}/${u_id}`);
}

export function connect() {
  return select('');
}

export function UserPhoto(data: any) {
  return fetch(`${_url}/user/photo`, {
    method: 'post',
    body: data,
  })
    .then(res => res.json())
    .catch(er => {
      console.log('error', er);
    })
    .then(data => {
      return data;
    });
}

export function UserChange(data: any) {
  return fetch(`${_url}/user/edit`, {
    method: 'post',
    body: data,
  })
    .then(res => res.json())
    .catch(er => {
      console.log('error', er);
    })
    .then(data => {
      return data;
    });
}

export function getIventUserById(id: number) {
  return select(`ivents/party/${id}`);
}

function select(call: string, body: any = { option: 'get' }) {
  const url = `${_url}/${call}`;
  return fetch(url, {
    ...body,
  })
    .then(res => res.json())
    .catch(er => {
      console.log('error', er);
    })
    .then(data => {
      return data;
    });
}
